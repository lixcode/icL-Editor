#include "plugin.h"

#include "../self/cursorsarea.h"
#include "../self/editorinternal.h"
#include "../self/linenumbers.h"
#include "../self/opacitymask.h"

EditorPlugin::EditorPlugin(QObject * parent)
    : QQmlExtensionPlugin(parent) {}

void EditorPlugin::registerTypes(const char * uri) {
    Q_ASSERT(uri == QLatin1String("icL.Editor"));

    qmlRegisterAnonymousType<icL::editor::Logic>(uri, 1);
    qmlRegisterAnonymousType<icL::editor::Drawing>(uri, 1);
    qmlRegisterAnonymousType<icL::editor::History>(uri, 1);
    qmlRegisterAnonymousType<icL::editor::Keyboard>(uri, 1);
    qmlRegisterAnonymousType<icL::editor::Mouse>(uri, 1);
    qmlRegisterType<icL::editor::EditorInternal>(uri, 1, 0, "EditorInternal");
    qmlRegisterType<icL::editor::LineNumbers>(uri, 1, 0, "LineNumbers");
    qmlRegisterType<icL::editor::OpacityMask>(uri, 1, 0, "EditorOpacityMask");
    qmlRegisterType<icL::editor::CursorsArea>(uri, 1, 0, "CursorsArea");
}
