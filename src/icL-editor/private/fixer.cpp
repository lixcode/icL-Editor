#include "fixer.h"

#include "../self/logic.h"
#include "line.h"

namespace icL::editor {

Fixer::Fixer() {
    linePtr.storeRelease(nullptr);
}

void Fixer::fix(Line * line) {
    if (
      linePtr.loadAcquire() == nullptr || !isRunning() ||
      linePtr.loadAcquire()->lineNumber() >= line->lineNumber()) {
        linePtr.storeRelease(line);
    }

    if (!isRunning()) {
        start(LowPriority);
    }
}

void Fixer::fixNow(Line * line) {
    auto * it = line;

    while (it->next() != nullptr) {
        fixOne(it);
        it = it->next();
    }

    fixOne(it);
    line->parent()->lChangeNumberOfLines(it->lineNumber());
}

void Fixer::run() {
    while (linePtr.loadAcquire()->next() != nullptr) {
        fixOne(linePtr.loadAcquire());

        linePtr.storeRelease(linePtr.loadAcquire()->next());
    }

    auto * line = linePtr.loadAcquire();

    fixOne(line);
    line->parent()->lChangeNumberOfLines(line->lineNumber());
}

void Fixer::fixOne(Line * line) {
    if (line->prev() == nullptr) {
        line->setBeginPos(0);
        line->setLineNumber(1);
    }
    else {
        line->setLineNumber(line->prev()->lineNumber() + 1);
        line->setBeginPos(
          line->prev()->beginPos() + line->prev()->length() + 1);
    }
}

}  // namespace icL::editor
