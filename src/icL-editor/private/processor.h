#ifndef ICL_EDITOR_PROCESSOR_H
#define ICL_EDITOR_PROCESSOR_H

#include <icL-editor/self/logic.h>
#include <icL-shared/main/engine.h++>

#include <QThread>



namespace icL::editor {



class Processor : public QThread
{
    core::shared::Engine engine;

public:
    Processor(Logic * editor);

    // QThread interface
protected:
    void run() override;

private:
    class VM * vm;

    Logic * editor;
};

}  // namespace icL::editor

#endif // ICL_EDITOR_PROCESSOR_H
