#include "processor.h"

#include "line.h"

#include <icL-il/structures/code-fragment.h++>

#include <icL-shared/main/engine.h++>
#include <icL-vm/vmstack.h++>



namespace icL::editor {

class VM : public core::vm::VMStack
{
    Line * line;

public:
    VM(core::il::InterLevel * il)
        : core::vm::VMStack(il) {}

    void setLine(Line * line) {
        this->line = line;
    }

    // VMStack interface
public:
    void markError(
      const core::il::CodeFragment & /*code*/,
      const icString & /*error*/) override {
        //
    }

    void markToken(const core::il::CodeFragment & code, int color) override {

        qDebug() << code.begin.line << code.begin.relative << code.end.line
                 << code.end.relative << color;

        while (code.begin.line < line->lineNumber()) {
            line = line->prev();
        }

        while (code.begin.line > line->lineNumber()) {
            line = line->next();
        }

        if (code.begin.line == code.end.line) {
            line->markToken(
              code.begin.relative - 1, code.end.relative - 1, color);
        }
        else {
            line->markToken(code.begin.relative - 1, line->length(), color);
            line = line->next();

            while (code.end.line > line->lineNumber()) {
                line->markToken(0, line->length(), color);
                line = line->next();
            }

            line->markToken(0, code.end.relative - 1, color);
        }
    }
};

Processor::Processor(Logic * editor)
    : editor(editor) {
    vm = new VM(engine.il());
    engine.setVMStack(vm);
    engine.il()->mode    = core::il::Mode::Check;
    engine.il()->submode = core::il::Submode::Colorize;
    engine.finalize();
}

void Processor::run() {
    vm->init("main.icL");
    vm->setLine(editor->first());
    vm->run();
}

}  // namespace icL::editor
