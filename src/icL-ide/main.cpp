#include "gateway.h"

#include <icL-look/export/look.h>

#include <iostream>

#include <QDebug>
#include <QGuiApplication>
#include <QObject>
#include <QOpenGLContext>
#include <QQmlApplicationEngine>
#include <QQmlContext>
#include <QQmlEngine>
#include <QSurface>


int main(int argc, char * argv[]) {
    QGuiApplication app(argc, argv);

    // Now we are using a single engine
    // Thnaks to derM for his/her answer
    // https://stackoverflow.com/questions/52696330/how-to-create-some-independent-windows-in-qml/52699869
    QQmlApplicationEngine engine;

    QQmlContext * context = engine.rootContext();

    icL::ide::GateWay gateway;

    context->setContextProperty("gateway", &gateway);

    qmlRegisterSingletonType(
      {"qrc:/utils/MoveFlags.qml"}, "icL", 1, 0, "MoveFlags");

    engine.load("qrc:/windows/start-window.qml");
    engine.load("qrc:/main.qml");

    return QGuiApplication::exec();
    return 0;
}
