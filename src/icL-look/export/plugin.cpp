#include "plugin.h"

#include "../base/effectadv.h"
#include "../base/linkadv.h"
#include "../editor/change.h"
#include "../editor/charformat.h"
#include "../editor/editor.h"
#include "../editor/editorstyle.h"
#include "../editor/highlight.h"
#include "../editor/line.h"
#include "../session/centralside.h"
#include "../session/floating.h"
#include "../session/issue.h"
#include "../session/leftside.h"
#include "../session/sessionwindow.h"
#include "../session/static.h"
#include "../session/topbar.h"
#include "../session/tree.h"
#include "../start/input.h"
#include "../start/side.h"
#include "../start/startwindow.h"
#include "chars.h"
#include "look.h"

#include <QQmlEngine>
#include <QtDebug>

LookPlugin::LookPlugin(QObject * parent)
    : QQmlExtensionPlugin(parent) {}

void LookPlugin::registerTypes(const char * uri) {
    Q_ASSERT(uri == QLatin1String("icL.Look"));

    qmlRegisterAnonymousType<icL::look::TextLook>(uri, 1);
    qmlRegisterAnonymousType<icL::look::Effect>(uri, 1);
    qmlRegisterAnonymousType<icL::look::EffectAdv>(uri, 1);
    qmlRegisterAnonymousType<icL::look::Link>(uri, 1);
    qmlRegisterAnonymousType<icL::look::LinkAdv>(uri, 1);
    qmlRegisterAnonymousType<icL::look::CharFormatBase>(uri, 1);
    qmlRegisterAnonymousType<icL::look::CharFormat>(uri, 1);
    qmlRegisterAnonymousType<icL::look::Editor>(uri, 1);
    qmlRegisterAnonymousType<icL::look::Highlight>(uri, 1);
    qmlRegisterAnonymousType<icL::look::Line>(uri, 1);
    qmlRegisterAnonymousType<icL::look::CentralSide>(uri, 1);
    qmlRegisterAnonymousType<icL::look::Floating>(uri, 1);
    qmlRegisterAnonymousType<icL::look::Issue>(uri, 1);
    qmlRegisterAnonymousType<icL::look::LeftSide>(uri, 1);
    qmlRegisterAnonymousType<icL::look::SessionWindow>(uri, 1);
    qmlRegisterAnonymousType<icL::look::TopBar>(uri, 1);
    qmlRegisterAnonymousType<icL::look::Tree>(uri, 1);
    qmlRegisterAnonymousType<icL::look::Input>(uri, 1);
    qmlRegisterAnonymousType<icL::look::ListItem>(uri, 1);
    qmlRegisterAnonymousType<icL::look::Side>(uri, 1);
    qmlRegisterAnonymousType<icL::look::StartWindow>(uri, 1);
    qmlRegisterAnonymousType<icL::look::Static>(uri, 1);
    qmlRegisterAnonymousType<icL::look::EditorStyle>(uri, 1);
    qmlRegisterAnonymousType<icL::look::Chars>(uri, 1);
    qmlRegisterAnonymousType<icL::look::ScrollBar>(uri, 1);
    qmlRegisterAnonymousType<icL::look::Change>(uri, 1);
    qmlRegisterType<icL::look::Look>(uri, 1, 0, "Look");
}
