#-------------------------------------------------
#
# Project created by QtCreator 2017-08-16T12:56:38
#
#-------------------------------------------------

TEMPLATE = subdirs

SUBDIRS = \
        core \
        look \
        toolkit \
        editor \
        ide

#sub-module
core.subdir   = src/icL-Core

# libs
editor.subdir   = src/icL-editor
look.subdir     = src/icL-look
toolkit.subdir  = src/icL-toolkit

# apps
ide.subdir      = src/icL-ide

# depends
look.depends = editor
ide.depends = look
